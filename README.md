# Java版本DDNS

#### 介绍
把服务厂商的域名修改为本机域名，可以编译成jar包，然后配合操作系统的定时任务启动。
Java版本

#### 环境说明
要运行需要安装三个软件
1. JDK
2. git
3. maven


#### 开发说明

一：开发模式
1.  下载源代码
进入命令控制台
```
git clone https://gitee.com/harley-dog/ddns4java.git
```
2.  修改配置文件  
src/main/resources/ddns.xml  
`按照配置相关说明修改为自己的配置`
3.  启动  
运行net.quanter.ddns4java.Ddns4JavaStarter


#### 正式环境使用说明

1. 下载源代码
进入命令控制台
```
git clone https://gitee.com/harley-dog/ddns4java.git
```

2. 编译
进入代码目录，运行mvn package

```
cd /root/ddns4java
mvn package
```

3. （不是必须）拷贝jar到指定目录下
```
cp target/ddns4java.jar /root/ddns
```
4. 拷贝配置文件
拷贝配置文件到指定目录下，建议重命名  
```
cp /root/ddns4java/ddns-sample.xml /root/ddns/ddns.xml
```
5. 修改配置文件
6. 启动
```
cd /root/ddns/ && java -jar ddns4java.jar ddns.xml
```
第一个参数就是配置文件的路径


#### 配置文件说明
```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<ddnsconfig>
    <providers>
        <!--域名提供商为阿里云-->
        <provider value="aliyun">
            <!--这里输入你的阿里云accesskey和accessSecret-->
            <accessKey>阿里云accessKey</accessKey>
            <accessSecret>阿里云accessAppSecret</accessSecret>
        </provider>
    </providers>
    <domains>
        <!--可以添加多个域名-->
        <domain name="quanter.net">
            <prefixs>
                <!--可以添加多个域名的前缀，这些前缀都会被更新成本机ip--->
                <prefix>www</prefix>
                <prefix>service</prefix>
            </prefixs>
        </domain>
        <domain name="harley-dog">
            <prefixs>
                <prefix>www</prefix>
                <prefix>service</prefix>
            </prefixs>
        </domain>
    </domains>
</ddnsconfig>

```


#### 定时任务说明
在linux环境下，运行crontab -e进入定时任务的编辑界面
设置每10分钟运行一次
```
*/10 * * * * cd /root/ddns && java -jar ddns4java.jar /root/ddns/ddns.xml
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
