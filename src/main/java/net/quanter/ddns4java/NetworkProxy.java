package net.quanter.ddns4java;

/**
 * 网络接口
 * @author custer
 */
public interface NetworkProxy {
    /**
     * 获取本机公网ip
     * @return
     */
    String getPublicIp();
}
