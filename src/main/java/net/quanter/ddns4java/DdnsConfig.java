package net.quanter.ddns4java;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

/**
 * @author custer
 */
@Getter
public class DdnsConfig {
    List<Provider> providerList = new ArrayList<>();
    List<Domain> domainList = new ArrayList<>();
}
