package net.quanter.ddns4java;

import lombok.Getter;
import net.quanter.ddns4java.enums.RecordType;

/**
 * 域名解析记录
 * @author custer
 */
@Getter
public class DomainRecord {
    final String id;
    final String domain;
    final String prefix;
    final String value;
    final RecordType type;

    public DomainRecord(String id,String domain,String prefix,String value,String type){
        this.id = id;
        this.domain = domain;
        this.prefix = prefix;
        this.value = value;
        this.type = RecordType.valueOf(type);
    }
}
