package net.quanter.ddns4java.enums;

/**
 * 解析类型
 * @author custer
 */

public enum RecordType {
    /**IPV4地址*/
    A,
    /*指向另一个域名*/
    CNAME,
    /*IPV6地址*/
    AAAA,
    /*其他DNS服务器*/
    NS,
    /*邮件服务器*/
    MX,
    /*特定的服务器提供商*/
    SRV,
    /*文本*/
    TXT,
    /*CA证书颁发机构*/
    CAA,

}
