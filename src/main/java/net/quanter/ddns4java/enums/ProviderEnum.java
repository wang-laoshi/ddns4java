package net.quanter.ddns4java.enums;

import net.quanter.ddns4java.Provider;
import net.quanter.ddns4java.providers.AliyunProvider;

/**
 * @author harley-dog
 */

public enum ProviderEnum {
    aliyun(new AliyunProvider());
    public Provider provider;

    ProviderEnum(Provider provider) {
        this.provider = provider;
    }
}
