package net.quanter.ddns4java;

import java.util.List;

/**
 * DDNS抽象接口
 * @author custer
 */
public interface Provider {
    /**
     * 初始化
     * @return 初始化是否成功
     */
    boolean init();

    /**
     * 是否拥有该域名
     * @param domain 域名
     * @return 是=true，否=false
     */
    boolean hasDomain(String domain);

    /**
     * 看看有哪些解析
     * @param domain 域名名称
     * @return 解析列表
     */
    List<DomainRecord> findRecordsByDomain(String domain);

    /**
     * 修改域名解析记录
     * @param updateRecord 域名解析记录
     * @return 是否修改成功
     */
    boolean updateRecord(DomainRecord updateRecord);

    /**
     * 新增域名解析记录
     * @param insertRecord
     * @return
     */
    boolean insertRecord(DomainRecord insertRecord);

}
