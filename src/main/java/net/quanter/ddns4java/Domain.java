package net.quanter.ddns4java;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * 域名实体类
 * @author custer
 */
public class Domain {
    /**
     * 域名
     */
    @Setter @Getter
    private String name;
    /**
     * 域名前缀
     */
    @Getter
    private Set<String> prefixs = new HashSet<>();
    public void addPrefix(String prefix){
        prefixs.add(prefix);
    }

    public boolean valid() {
        return name!=null && !prefixs.isEmpty();
    }

}
