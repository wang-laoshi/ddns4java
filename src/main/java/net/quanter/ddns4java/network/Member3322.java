package net.quanter.ddns4java.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import lombok.extern.slf4j.Slf4j;
import net.quanter.ddns4java.NetworkProxy;

/**
 * 用http://members.3322.org/dyndns/getip获取公网ip
 * @author custer
 */
@Slf4j
public class Member3322 implements NetworkProxy {
    @Override
    public String getPublicIp() {
        InputStream in = null;
        URL httpUrl = null;
        HttpURLConnection urlConnection = null;
        try {
            httpUrl = new URL("http://members.3322.org/dyndns/getip");
            urlConnection = (HttpURLConnection)httpUrl.openConnection();
            in = urlConnection.getInputStream();
            byte[] b = new byte[in.available()];
            in.read(b);
            return new String(b).replaceAll("\n","").trim();
        } catch (IOException e) {
            log.info("访问http://members.3322.org/dyndns/getip出错");
            return null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {

                }
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
}
